package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class UserServiceApplicationTests {
	@Autowired
	private UserService userService;

	@Test
	public void testSaveUser() {
		User user = new User((long)1, "Test User", "Test User Address", "Test User Code");
		User testUser = userService.saveUser(user);
		assertThat(testUser.getUserName()).isEqualTo("Test User"); 
	}

	@Test
	public void testGetUser() {
		User user = new User((long)1, "Test User", "Test User Address", "Test User Code");
		userService.saveUser(user);
		User testUser = userService.findUserById((long)1);
		assertThat(testUser.getUserName()).isEqualTo("Test User");
	}
}
